# 🍖 Delicatessen
## The serverless app for Delicatessen management and food processing

Simple JS app where you can follow the process of processing charcuterie.
Let you manage products, rooms, and processes, display how much time's left 
before you can consume the product. 