/**
 * @author : Gaël DIGARD <gaeldigard@gmail.com>
 * @date : 2020-11-01
 * @desc : Useful functions related to anything (like loading sample data, erasing all memory, etc)
 *
 */

import Room from "./models/Room.js";
import Product from "./models/Product.js";
import Process from "./models/Process.js";


export default class Tools{

    static notificationFile = '../assets/sound/notification.mp3';

    constructor(){
        
    }

    static export(){

        // get all local storage
        let data = {};

        let room = new Room;
        data['rooms']       = JSON.parse(localStorage.getItem( room.localStorageKey ));

        let product = new Product;
        data['products']    = JSON.parse(localStorage.getItem( product.localStorageKey ));

        let process = new Process;
        data['processes']   = JSON.parse(localStorage.getItem( process.localStorageKey ));

        //download file
        this.download( 'backup-delicatessen-'+luxon.DateTime.local().toSQLDate()+'.txt', JSON.stringify(data) )
    }

    static import( data ){
        var data = JSON.parse(data);
        Object.keys(data).forEach(function (k) {
            localStorage.setItem(k, JSON.stringify(data[k]));
        });
    }

    /**
     * @desc Force the download of a file on the fly
     * @param filename string Output filename : myfile.txt
     * @param text mixed Content to put inside the file
     */
    static download(filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + (text));
        element.setAttribute('download', filename);

        element.style.display = 'none';
        document.body.appendChild(element);

        element.click();

        document.body.removeChild(element);
    }



    /**
     * @desc Remove all data saved, by erasing localStorage
     */
    static eraseAllData(){
        localStorage.clear();
    }

    /**
     * @desc Feed app with fake but coherent data
     */
    static seed(){

        // clean all first
        this.eraseAllData();

        // use now time in process list
        let now = luxon.DateTime.local();
        
        // Seed rooms
        let room = new Room;
        let ar = [];

        let obj = {};
        obj.name = "Fermentation room";
        obj.id = 1;
        ar.push(obj)

        obj = {};
        obj.name = "Curing room";
        obj.id = 2;
        ar.push(obj)

        obj = {};
        obj.name = "Cave";
        obj.id = 3;
        ar.push(obj)

        obj = {};
        obj.name = "Fridge";
        obj.id = 4;
        ar.push(obj)

        localStorage.setItem( room.localStorageKey, JSON.stringify(ar) );

        // Seed products
        let product = new Product;
        ar = [];

        obj = {};
        obj.name = "Pancetta";
        obj.id = 1;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)        

        obj = {};
        obj.name = "Saucisson sec";
        obj.id = 2;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Guanciale";
        obj.id = 3;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Ventrèche";
        obj.id = 4;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Jambon de cerf";
        obj.id = 5;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Pastrami de boeuf";
        obj.id = 6;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Mortadelle";
        obj.id = 7;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Pâté de campagne";
        obj.id = 8;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Magret de canard séché";
        obj.id = 9;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)

        obj = {};
        obj.name = "Grison de boeuf";
        obj.id = 10;
        obj.recipe = "Lorem ipsum dolor sit amet";
        ar.push(obj)


        localStorage.setItem( product.localStorageKey, JSON.stringify(ar) );

        // Seed processes 
        let process = new Process;
        let processes = '\
        [\
            {\
               "id":1223343,\
               "product":1,\
               "qty":4,\
               "room":1,\
               "duration":3,\
               "time_unit":"h",\
               "batch":"A9UE8",\
               "created_at":"'+now.minus({'hours':1}).toString()+'"\
            },\
            {\
               "id":134024,\
               "product":2,\
               "qty":5,\
               "room":2,\
               "duration":15,\
               "time_unit":"d",\
               "batch":"97CBA3",\
               "created_at":"'+now.minus({'days':12}).toString()+'"\
            },\
            {\
               "id":414141,\
               "product":3,\
               "qty":2,\
               "room":2,\
               "duration":9,\
               "time_unit":"d",\
               "batch":"MLKSD9",\
               "created_at":"'+now.minus({'days':2}).toString()+'"\
            },\
            {\
               "id":100332,\
               "product":4,\
               "qty":4,\
               "room":3,\
               "duration":30,\
               "time_unit":"d",\
               "batch":"VVEN334",\
               "created_at":"'+now.minus({'days':23}).toString()+'"\
            },\
            {\
               "id":2432013,\
               "product":5,\
               "qty":1,\
               "room":4,\
               "duration":10,\
               "time_unit":"d",\
               "batch":"",\
               "created_at":"'+now.minus({'days':1}).toString()+'"\
            },\
            {\
               "id":124043,\
               "product":6,\
               "qty":1,\
               "room":1,\
               "duration":8.25,\
               "time_unit":"h",\
               "batch":"",\
               "created_at":"'+now.minus({'hours':1.34}).toString()+'"\
            },\
            {\
               "id":2101222,\
               "product":7,\
               "qty":10,\
               "room":3,\
               "duration":10,\
               "time_unit":"d",\
               "batch":"MMOR23",\
               "created_at":"'+now.minus({'days':4}).toString()+'"\
            },\
            {\
               "id":2200144,\
               "product":8,\
               "qty":5,\
               "room":4,\
               "duration":3,\
               "time_unit":"d",\
               "batch":"PATE_CAMPAGNE_22",\
               "created_at":"'+now.minus({'days':1}).toString()+'"\
            },\
            {\
               "id":241212,\
               "product":9,\
               "qty":3,\
               "room":4,\
               "duration":15,\
               "time_unit":"d",\
               "batch":"MAGRET-SECHE",\
               "created_at":"'+now.minus({'days':10}).toString()+'"\
            },\
            {\
               "id":3020213,\
               "product":10,\
               "qty":5,\
               "room":1,\
               "duration":10,\
               "time_unit":"d",\
               "batch":"GGRZON",\
               "created_at":"'+now.minus({'days':3.4}).toString()+'"\
            }\
         ]\
        ';
        localStorage.setItem( process.localStorageKey, processes );

    }

    static playNotification(){
        let audio = new Audio(this.notificationFile);
        audio.play().then(r => console.log('played'));
    }
  
    // Always escape HTML for text arguments!
    static escapeHtml(html) {
      const div = document.createElement('div');
      div.textContent = html;
      return div.innerHTML;
    }  
  
    // Custom function to emit toast notifications
    static notify(message, type = 'primary', icon = 'info-circle', duration = 3000) {
      const alert = Object.assign(document.createElement('sl-alert'), {
        type: type,
        closable: true,
        duration: duration,
        innerHTML: `
          <sl-icon name="${icon}" slot="icon"></sl-icon>
          ${this.escapeHtml(message)}
        `
      });
  
      document.body.append(alert);
      return alert.toast();
    }
  
    
}