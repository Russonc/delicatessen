/**
 * @author : Gaël DIGARD <gaeldigard@gmail.com>
 * @date : 2020-10-22
 * @desc : Data model for a product being in a room, processed
 *
 */

import Product from "./Product.js";
import Room    from "./Room.js";

export default class Process {

    constructor() {
        this.localStorageKey = 'processes';
        this.ProductInstance = new Product();
        this.RoomInstance = new Room();
    }

    /**
     * Get processes list
     * @returns {string}
     */
    get processes() {

        let processes = JSON.parse(localStorage.getItem(this.localStorageKey));
        let products = this.ProductInstance.products;
        let rooms = this.RoomInstance.rooms;

        if (processes != null) {
            for (let process of processes) {

                // get product object
                for (let p of products) {
                    if (p.id == parseInt(process.product)) {
                        process.product = p;
                    }
                }

                // get room object
                for (let r of rooms) {
                    if (r.id == parseInt(process.room)) {
                        process.room = r;
                    }
                }
            }
        }
        return processes;
    }

    /**
     *
     * @param {Number} processID Process ID to delete
     */
    trash( processID ){
        let newProcesses = [];
        let processes = JSON.parse(localStorage.getItem(this.localStorageKey));
        if (processes != null) {
            for (let process of processes) {
                if( process.id != processID ){
                    newProcesses.push(process)
                }
            }
            localStorage.setItem(this.localStorageKey, JSON.stringify(newProcesses));
            return 1;
        }
    }

    /**
     * Stores a process in local storage
     * @param data
     * @returns {number}
     */
    create(process) {
        if (process.product == '' || process.duration == '' || process.time_unit == '' || process.created_at == '')
            return -1;

        // Retreive already stored data
        let processes = JSON.parse(localStorage.getItem(this.localStorageKey)) || []
        // Add new one
        processes.push(process)
        localStorage.setItem(this.localStorageKey, JSON.stringify(processes));
        return 1;
    }

    /**
     * Return a bunch of time and duration relative to the given process
     * @param {object} process
     * @return {object} Standard object full of metrics
     */
    getProgressMetrics(process) {

        let o = {};
        let end;

        let now = luxon.DateTime.local();
        o.begining = luxon.DateTime.fromISO(process.created_at)

        if (process.time_unit == 'd') {
            o.end = o.begining.plus({days: process.duration});
        } else if (process.time_unit == 'h') {
            o.end = o.begining.plus({hours: process.duration});
        } else {
            alert('Time_unit unknown')
        }

        o.totalDuration = luxon.Interval.fromDateTimes(o.begining, o.end);
        o.durationRemaining = luxon.Interval.fromDateTimes(now, o.end);
        o.durationMade = luxon.Interval.fromDateTimes(o.begining, now);
        o.percentageDone = 100 / (o.totalDuration.length('seconds') / o.durationMade.length('seconds'));
        o.percentageRemaining = 100 / (o.totalDuration.length('seconds') / o.durationRemaining.length('seconds'));

        if (o.end.toMillis() <= now.toMillis()) {
            o.percentageDone = 100;
            o.percentageRemaining = 0;
        }

        return o;

    }
}

