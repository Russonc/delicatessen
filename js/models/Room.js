/**
 * @author : Gaël DIGARD <gaeldigard@gmail.com>
 * @date : 2020-10-07
 * @desc : Data model for rooms containing products
 *
 */


export default class Room{

    constructor(){
        this.localStorageKey = 'rooms';
    }

    /**
     * Get rooms list with processes inside
     * @returns {string}
     */
    get rooms(){

        let rooms = JSON.parse(localStorage.getItem( this.localStorageKey ));
        return rooms;

    }

    /**
     * Store a room in local storage
     * @param product
     * @returns {number}
     */
    create( room ){
        if(room.name=='')
            return -1;
        // Retreive already stored data
        let rooms = JSON.parse(localStorage.getItem(this.localStorageKey)) || []
        // Add new one
        rooms.push(room)
        localStorage.setItem( this.localStorageKey,  JSON.stringify(rooms) );
        return 1;
    }

}

