/**
 * @author : Gaël DIGARD <gaeldigard@gmail.com>
 * @date : 2020-10-07
 * @desc : Data model for delicatessen app
 *
 */


export default class Product{

    constructor(){
        this.localStorageKey = 'products';
    }

    /**
     * Get products list
     * @returns {string}
     */
    get products(){
        return JSON.parse(localStorage.getItem( this.localStorageKey ));
    }

    /**
     * Stores a product in local storage
     * @param data
     * @returns {number}
     */
    create( product ){
        if(product.name=='')
            return -1;
        // Retreive already stored data
        let products = JSON.parse(localStorage.getItem(this.localStorageKey)) || []
        // Add new one
        products.push(product)
        localStorage.setItem( this.localStorageKey,  JSON.stringify(products) );
        return 1;
    }

}
