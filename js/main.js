
import Room from "./models/Room.js?v=1";
import Product from "./models/Product.js?v=1";
import Process from "./models/Process.js?v=1";
import Vue from "./View.js?v=1";
import Tools from "./Tools.js?v=1";

// Document ready i guess
(() => {

    let RoomInstance   = new Room();
    let ProductInstance = new Product();
    let ProcessInstance = new Process();

    /**
     * DOM elements selection
     */
    const wrapperRooms      = document.querySelector('#wrapper-rooms');
    const wrapperProducts   = document.querySelector('#wrapper-products');
    const wrapperProcesses  = document.querySelector('#wrapper-overview');
    const selectRooms       = document.querySelector('sl-select.select-room');
    const selectProducts    = document.querySelector('sl-select.select-product');
    const inputFile = document.getElementById('input');
    const selectedFile = inputFile.files[0];



    if(RoomInstance.rooms) {

        wrapperRooms.innerHTML = '';

        RoomInstance.rooms.forEach(e => Vue.renderRoomCard(e, ProcessInstance.processes));

        RoomInstance.rooms.forEach(p => Vue.renderItemSelect(p, selectRooms));
    }

    if(ProductInstance.products) {

        wrapperProducts.innerHTML = '';


        ProductInstance.products.forEach(p => Vue.renderProductCard(p));

        ProductInstance.products.forEach(p => Vue.renderItemSelect(p, selectProducts));
    }

    if(ProcessInstance.processes) {
            wrapperProcesses.innerHTML = '';
            ProcessInstance.processes.forEach(p => Vue.renderProcessCard( p, ProcessInstance.getProgressMetrics(p)));
            setInterval( function(){
                ProcessInstance.processes.forEach(p => Vue.refreshProgressBar( p, ProcessInstance.getProgressMetrics(p)));

            }, 1000)

    }



    /*
    * Add product
    * */

    //
    // Dialog
    //
    const dialog = document.querySelector('.dialog-overview-product');
    const openButtonAddProduct = document.querySelector('#add-product-button');
     openButtonAddProduct.addEventListener('click', () => dialog.show());



    //
    // Save Product
    //
    const form = document.querySelector('.form-add-product');
    // Watch for the slSubmit event
    form.addEventListener('sl-submit', event => {

        const formData = event.detail.formData;
        let o = {};
        o.id = uuid();
        for (const entry of formData.entries()) {
            o[entry[0]] = entry[1]

        }
        console.log(o)
        if( 1 == ProductInstance.create( o ) ){
            let b = document.querySelector('#save-product');
            b.setAttribute('name','check')

            // clear product list
            wrapperProducts.innerHTML = '';

            // re-render it
            ProductInstance.products.forEach(p => Vue.renderProductCard(p));

            // re-render <select>
            ProductInstance.products.forEach(p => Vue.renderItemSelect(p, selectProducts));

        }

    });

    /*
    * Add room
    * */

    //
    // Dialog
    //
    const dialogRoom = document.querySelector('.dialog-overview-room');
    const openButtonAddRoom = document.querySelector('#add-room-button');
    openButtonAddRoom.addEventListener('click', () => dialogRoom.show());

    //
    // Save Room
    //
    const formRoom = document.querySelector('.form-add-room');
    // Watch for the slSubmit event
    formRoom.addEventListener('sl-submit', event => {

        const formData = event.detail.formData;
        let o = {};
        o.id = uuid();
        for (const entry of formData.entries()) {
            o[entry[0]] = entry[1]

        }

        if( 1 == RoomInstance.create( o ) ){
            let b = document.querySelector('#save-room');
            b.setAttribute('name','check')

            // clear product list
            wrapperRooms.innerHTML = '';

            // re-render it
            RoomInstance.rooms.forEach(p => Vue.renderRoomCard(p));

            // re-render <select>
            RoomInstance.rooms.forEach(p => Vue.renderItemSelect(p, selectRooms));

        }

    });

    /*
   * Add product in room
   * */

    //
    // Dialog
    //
    const dialogProductRoom = document.querySelector('.dialog-overview-product-room');
    const openButtonAddProductRoom = document.querySelector('#add-product-room-button');
    openButtonAddProductRoom.addEventListener('click', () => dialogProductRoom.show());

    //
    // Save product in room
    //
    const formProductRoom = document.querySelector('.form-add-product-room');

    // Watch for the slSubmit event
    formProductRoom.addEventListener('sl-submit', event => {

        const formData = event.detail.formData;
        let o = {};
        o.id = uuid();
        for (const entry of formData.entries()) {
            o[entry[0]] = entry[1]

        }

        o.qty = parseInt(o.qty);
        o.product = parseInt(o.product);
        o.room = parseInt(o.room);
        o.duration = parseFloat(o.duration);
        o.created_at = new Date();

        if( 1 == ProcessInstance.create( o ) ){
            let b = document.querySelector('#save-product-room');
            b.setAttribute('name','check')

            // clear product list
            wrapperProcesses.innerHTML = '';

            // re-render it
            ProcessInstance.processes.forEach(p => Vue.renderProcessCard( p, ProcessInstance.getProgressMetrics(p)));
        }
    });

    /**
     * Actions process button
     */
    const dropdownAction = document.querySelectorAll('sl-dropdown#process-actions');

    dropdownAction.forEach(element => {

        element.addEventListener('sl-select', event => {
            const selectedItem = event.detail.item;

            //
            // Delete process
            //
            if(selectedItem.value == 'erase'){
                let processID = selectedItem.getAttribute('data-process-id');
                if(processID){
                    if(confirm('Are you sure you want to delete this process ?')){
                        if(ProcessInstance.trash( processID )){
                            const wrapper = document.querySelector('#wrapper-overview');
                                wrapper.innerHTML = '';
                            ProcessInstance.processes.forEach(p => Vue.renderProcessCard( p, ProcessInstance.getProgressMetrics(p)));

                        }
                        else{
                            alert('Error happened while deleting process #' + processID)
                        }
                    }
                }
            }
            //
            // Edit process
            //
            else if(selectedItem.value == 'edit'){
                // @todo 
            }
        });
    });

    /**
     * Settings button
     */
    const container = document.querySelector('nav#bottom-bar');
    const dropdown = container.querySelector('sl-dropdown#settings');
  
    dropdown.addEventListener('sl-select', event => {
      const selectedItem = event.detail.item;

      switch(selectedItem.value){
          case 'load':
              loadFakeData();
              break;
        case 'erase':
                eraseAllData();
                break;
        case 'import':
                importData(inputFile, selectedFile);
                break;
        case 'save':
            Tools.export();
                break;
        case 'play-notification':
                Tools.playNotification();
                Tools.notify(`Notification was played ! Did you hear ? `, 'success', 'check2-circle', Infinity);
                break;
            default:
                console.log(`Sorry, we are out of ${selectedItem.value}.`);
                
      }
      
    });

   

})();

function importData( inputFile, selectedFile ){
    if(confirm('If you import data, all current data will be erased. Are you sure ? ')){

        inputFile.click();
        inputFile.addEventListener("change", handleFiles, false);
        function handleFiles() {
            const fileList = this.files; /* now you can work with the file list */
            Tools.eraseAllData();
            for (var i = 0, f; f = fileList[i]; i++)
            {
                var reader = new FileReader();
                reader.onload = function(event)
                {
                    // NOTE: event.target point to FileReader
                    var contents = event.target.result;
                    var lines = contents.split('\n');

                    Tools.import(contents);
                    document.location.reload(true);
                };
                reader.readAsText(f);
            }
        }
    }
}

function eraseAllData(){
    if(confirm('Are you sure ? ')){
        Tools.eraseAllData();
        document.location.reload(true);
    }
}
function loadFakeData(){
    if(confirm('If you load sample data, all current data will be erased. Are you sure ? ')){
        Tools.seed();
        document.location.reload(true);
    }
}
/**
 * Return a unique ID
 * @returns {number}
 */
function uuid() {
     return parseInt(Math.floor((1 + Math.random()) * 0x10000)
            .toString(5)
            .substring(1));

}