/**
 * @author : Gaël DIGARD <gaeldigard@gmail.com>
 * @date : 2020-10-07
 * @desc : GUI manipulation
 *
 */

export default class Vue{

    /**
     * Display an sl-card with room information in
     * @param {object} room Room object
     * @param {Array<Process>} processes List of processes
     */
    static renderRoomCard( room, processes  ){
        const wrapper = document.querySelector('#wrapper-rooms');
        const roomTmpl = document.querySelector('#roomTmpl');

        let clone = document.importNode(roomTmpl.content, true);
        let avatar = clone.querySelector('sl-avatar')
        let header = clone.querySelector('div[slot="header"] span.name');
        let desc = clone.querySelector('div.desc');
        let totalQty = 0;

        if( null != processes ){
            let products = []
            for (let process of processes) {
                if(process.room.id == room.id){
                    process.product.qty = process.qty;
                    totalQty += parseInt(process.qty)
                    products.push(process.product)
                }
                //console.log(process)
            }
            //console.log(products)
            if(products.length==0){
                desc.textContent = "No product in this room yet. Add one in 'Overview' tab"
            }
            else{
                let type = 'primary';
                if( totalQty > 5 )
                    type = 'warning';
                if( totalQty > 10 )
                    type = 'danger';

                desc.innerHTML = '<sl-tag type="'+type+'">'+ totalQty +' products</sl-tag><sl-menu-divider></sl-menu-divider>'
                for(let product of products){

                    desc.innerHTML += product.name +' ' + product.qty + ' unit(s), '
                }
            }
        }
        else{
            desc.textContent = "No product in this room yet. Add one in 'Overview' tab"
        }
        avatar.setAttribute('initials', room.name.charAt(0));

        header.textContent = room.name;
        wrapper.appendChild(clone)
    }

    /**
     * Display a sl-card with product information in
     * @param {object} product
     */
    static renderProductCard( product ){
        const wrapper = document.querySelector('#wrapper-products');
        const roomTmpl = document.querySelector('#productTmpl');

        let clone = document.importNode(roomTmpl.content, true);
        let avatar = clone.querySelector('sl-avatar')

        let header = clone.querySelector('div[slot="header"] span.name');
        let desc = clone.querySelector('div.desc')

        avatar.setAttribute('initials', product.name.charAt(0));

        header.textContent = product.name;
        if(product.recipe != '')
            desc.textContent = product.recipe;
        else
            desc.innerHTML = '&nbsp; <sl-skeleton></sl-skeleton> &nbsp; ';
        wrapper.appendChild(clone)
    }

    /**
     * Display a sl-card with a process ring in it
     * @param {Process} process
     * @param {object} metrics
     */
    static renderProcessCard ( process, metrics ){
        const wrapper = document.querySelector('#wrapper-overview');

        const processTmpl = document.querySelector('#processTmpl');

        let clone  = document.importNode(processTmpl.content, true);
        let avatar = clone.querySelector('sl-avatar')
        let trash  = clone.querySelector('sl-menu-item[action="trashProcess"]');
        let header = clone.querySelector('div[slot="header"] span.name');
        let ring   = clone.querySelector('sl-progress-ring');
        let desc   = clone.querySelector('.desc');
        let room   = clone.querySelector('.room');

        avatar.setAttribute('initials', process.product.name.charAt(0));
        trash.setAttribute('data-process-id', process.id)

        let type = 'primary';
        if( process.qty > 5 )
            type = 'warning';
        if( process.qty > 10 )
            type = 'danger';

        room.innerHTML = '<sl-tag type="'+type+'">'+ process.qty +' unit(s) </sl-tag><sl-menu-divider></sl-menu-divider>'
        room.innerHTML +=   ' in ' +process.room.name;
        desc.textContent = 'Ready ';
        if(metrics.percentageDone < 100) {
            desc.textContent +=  metrics.end.toRelative({
                'round':false
            });

        }
        else{
            ring.setAttribute( 'style',"--indicator-color: green;" )
        }
        header.textContent = process.product.name;
        if( process.batch != '' ){
            header.innerHTML += '<br/>#'+process.batch;
        }

        ring.textContent = parseInt(metrics.percentageDone) + '%';
        ring.setAttribute('percentage', metrics.percentageDone  )
        ring.setAttribute('process', process.id  )
        wrapper.appendChild(clone)
    }

    static refreshProgressBar( process, metrics ){
        const ring = document.querySelector( 'sl-progress-ring[process="'+process.id+'"]' )
        ring.textContent = parseInt(metrics.percentageDone) + '%';
        ring.setAttribute('percentage', metrics.percentageDone  )
        ring.setAttribute('process', process.id  )
    }

    static renderItemSelect( item, elem ){
        elem.innerHTML += '<sl-menu-item value="'+item.id+'">' + item.name + '</sl-menu-item>  <sl-menu-divider></sl-menu-divider>';
    }
}